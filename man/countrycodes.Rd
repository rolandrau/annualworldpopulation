\name{countrycodes}
\alias{countrycodes}
\docType{data}
\title{
  All countries/regions/continents of the United Nations' World
  Population Prospects, 2019 Revision, and their associated codes.
}
\description{
  A data frame with 255 observations, one for each
  country/region/continent, and two variables. The first ('country') is
  the name of the country/region/continent. The second column denotes
  the numeric code used by the United Nations for
  country/region/continent.
}
\usage{data("countrycodes")}
\format{
  A data frame with 255 observations on the following 2 variables.
  \describe{
    \item{\code{country}}{a character vector}
    \item{\code{countrycode}}{a numeric vector}
  }
}
\details{
  Those codes are used to access the 4D array 'pop.array'. I thought it
  is better to use those codes than the actual name of the country as
  the latter is more error-prone.
}
\source{
  World Population Prospects (2019 Revision) by the United Nations
}
\references{
  United Nations, Department of Economic and Social Affairs, Population
  Division (2019). World Population Prospects 2019, Online
  Edition. Rev. 1.
}
\examples{
data(countrycodes)
head(countrycodes)
}
\keyword{datasets}
